FROM rustlang/rust:nightly

# install necessary stuff; avahi, and ssh such that we can log in and control avahi
RUN apt-get update -y
RUN DEBIAN_FRONTEND=noninteractive apt-get -qq install -y avahi-utils xorg-dev libxcb-shape0-dev libxcb-xfixes0-dev clang avahi-daemon libavahi-client-dev \
  && apt-get -qq -y autoclean \
  && apt-get -qq -y autoremove \
  && apt-get -qq -y clean

WORKDIR /usr/src/myapp

COPY ./ ./

RUN CARGO_UNSTABLE_SPARSE_REGISTRY=true cargo +nightly -Z sparse-registry update
RUN CARGO_UNSTABLE_SPARSE_REGISTRY=true cargo build
 
  
CMD ["/usr/src/myapp/target/debug/zeroconf-test"]
# CMD ["/bin/bash"]
